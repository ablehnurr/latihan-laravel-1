<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <div class="main">
        <form action="../welcome">
            <label for="fname">First Name :</label><br>
            <input type="text" id="fname" name="fname"><br>
            <label for="fname">Last Name :</label><br>
            <input type="text" id="lname" name="lname"><br><br>
            <label for="gender">Gender</label><br>
            <input type="radio" id="gender" value="laki-laki"/>
			Laki - Laki <br>
		    <input type="radio" id="gender" value="perempuan"/>
			Perempuan <br><br>
            <label for="nationality">Nationality</label><br>
            <select id="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Singapura">Singapura</option>
            </select><br><br>
            <label for="bahasa">Language Spoken</label><br>
            <input type="checkbox" id="bahasa">Indonesia <br>
            <input type="checkbox" id="bahasa">English  <br>
            <input type="checkbox" id="bahasa">Mandarin <br> <br>
            <label for="bio">Bio :</label><br>
            <textarea id="bio" cols="30" rows="10"></textarea><br>
            <input type="submit" value="Sign Up">
        </form>
    </div>
</body>
</html>